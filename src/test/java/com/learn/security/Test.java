package com.learn.security;

import org.apache.commons.codec.binary.Hex;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Test {


    @org.junit.Test
    public void test1() throws Exception {
        String value = "hello 水电费水电费第三方舒服舒服水电费是否是第三方水电费水电费";
        String algorithm = "md2";
        // 获取MessageDigest实例
        MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
        // 调用digest方法生成数字摘要
        byte[] digest = messageDigest.digest(value.getBytes());
        // 结果转换为16进制字符串（借助common-codec Hex类）
        String md2Hex = Hex.encodeHexString(digest);
        System.out.println(md2Hex);
        System.out.println(md2Hex.length());
    }


    @org.junit.Test
    public void test2() throws NoSuchAlgorithmException {
        String value = "hello";
        String algorithm = "SHA-1";
        MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
        byte[] digest = messageDigest.digest(value.getBytes());
        System.out.println(Hex.encodeHexString(digest));
    }

}
