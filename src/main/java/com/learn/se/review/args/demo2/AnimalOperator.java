package com.learn.se.review.args.demo2;

public class AnimalOperator {

    /*抽象类作为形参和返回值
    方法的形参是抽象类名，其实需要的是该抽象类的子类对象
    方法的返回值是抽象类名，其实返回的是该抽象类的子类对象*/
    public void userAnimal(Animal a){ //Animal a = new Cat();
        a.eat();
    }

    public Animal getAnimal(){
        Animal a=new Cat();
        return a;
    }
}
