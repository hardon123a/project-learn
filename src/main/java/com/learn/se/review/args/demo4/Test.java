package com.learn.se.review.args.demo4;

public class Test {
    public static void main(String[] args) {
        callMe1(new String[]{"a","b","c"});
        callMe2(new String[]{"a","b","c"});
        callMe2("a","b","c");
        replaceTest();

    }

    public static void callMe1(String[] args) {
        System.out.println(args.getClass() == String[].class);
        for (String s : args) {
            System.out.println(s);
        }
    }

    public static void callMe2(String ... args) {
        System.out.println(args.getClass() == String[].class);
        for (String s : args) {
            System.out.println(s);
        }
    }

    public static void replaceTest(){
        String target="com.hjp.demo.class";
        System.out.println("替换前："+target);
        target=target.replace(".class","");
        System.out.println("替换后："+target);
    }
}
