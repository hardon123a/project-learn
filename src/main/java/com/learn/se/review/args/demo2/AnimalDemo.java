package com.learn.se.review.args.demo2;

public class AnimalDemo {

    public static void main(String[] args) {

        //创建操作类对象，并调用方法
        AnimalOperator ao=new AnimalOperator();
        Animal a=new Cat();
        ao.userAnimal(a);

        Animal a2=ao.getAnimal(); //new Cat()
        a2.eat();



    }
}
