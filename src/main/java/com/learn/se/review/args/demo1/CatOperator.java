package com.learn.se.review.args.demo1;

public class CatOperator {

    //类名作为形参和返回值
    public void useCat(Cat c) {
        c.eat();
    }

    public Cat getCat(){
        Cat c=new Cat();
        return c;
    }
}
