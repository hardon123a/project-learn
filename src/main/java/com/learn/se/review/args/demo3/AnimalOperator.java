package com.learn.se.review.args.demo3;

public class AnimalOperator {

    /*接口作为形参和返回值
    方法的形参是接口名，其实需要的是该接口的实现类对象
    方法的返回值是接口名，其实返回的是该接口的实现类对象*/
    public void userAnimal(Animal a){
        a.eat();
    }

    public Animal getAnimal(){
        Animal a=new Cat();
        return a;
    }
}
