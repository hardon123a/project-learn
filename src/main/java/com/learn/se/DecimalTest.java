package com.learn.se;

import java.math.BigDecimal;

public class DecimalTest {
    public static void main(String[] args) {
        BigDecimal big01=new BigDecimal("16.19");
        BigDecimal big02=new BigDecimal("16.59");
        BigDecimal big03=new BigDecimal("16.89");

        System.out.println(big01.intValue());
        System.out.println(big02.intValue());
        System.out.println(big03.intValue());
    }
}
