package com.learn.se.abs;

public abstract class Animal {

    private int age;
    private String name;
    private final String city="北京";

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Animal(){

    }

    public Animal(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public void show() {
        age =40;
        System.out.println(age);
        System.out.println(name);
        System.out.println(city);
    }

    public abstract void eat();
}
