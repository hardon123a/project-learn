package com.learn.se.abs;

public class AnimalDemo {

    public static void main(String[] args) {
        //创建对象，调用方法
        Jumpping jump=new Cat();
        jump.jump();
        System.out.println("-------------------------------");

        Jumpping jumpDog=new Dog();
        jumpDog.jump();
        System.out.println("-------------------------------");

        Animal cat = new Cat();
        cat.setAge(22);
        cat.setName("花猫");
        cat.eat();
        cat.show();

        System.out.println("-------------------------------");
        Animal dog = new Dog();
        dog.setAge(25);
        dog.setName("大狗");
        dog.eat();
        dog.show();

        Cat cat2 = new Cat();
        cat2.setAge(22);
        cat2.setName("花猫");
        cat2.eat();
        cat2.show();
        cat2.jump();


        System.out.println("-------------------------------");
        Dog dog2 = new Dog();
        dog2.setAge(25);
        dog2.setName("大狗");
        dog2.eat();
        dog2.show();
    }
}
