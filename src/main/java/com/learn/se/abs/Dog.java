package com.learn.se.abs;

public class Dog extends Animal implements Jumpping{

    public Dog() {
    }

    public Dog(int age, String name) {
        super(age, name);
    }

    @Override
    public void eat() {
        System.out.println("狗吃骨头");
    }

    @Override
    public void jump() {
        System.out.println("狗可以跳高了");
    }
}
