package com.learn.se.abs.demo1;

public class AnimalDemo {

    public static void main(String[] args) {
        //多态
        //向上转型
        Animal a = new Cat();
        a.eat();

        //向下转型
        Cat c=(Cat)a;
        c.eat();
        c.playGame();

        Animal animal=new Cat();
        animal.eat();

        Animal animal2=new Animal();
        animal2.eat();

    }
}
