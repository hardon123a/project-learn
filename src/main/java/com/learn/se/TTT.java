package com.learn.se;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class TTT {

    Map<Character, int[]> vectorMap = new HashMap<Character, int[]>();
    int[] tempArray = null;

    public TTT(String source,String target ){

        for(Character sch:source.toCharArray()){
            if(vectorMap.containsKey(sch)){
                vectorMap.get(sch)[0]++;
            }
            //用将字符转化为向量
            else{
                tempArray = new int[2];
                tempArray[0] = 1;
                tempArray[1] = 0;
                vectorMap.put(sch, tempArray);
            }

        }

        for(Character tch:target.toCharArray()){
            if(vectorMap.containsKey(tch)){
                vectorMap.get(tch)[1]++;
            }
            //用将字符转化为向量
            else{
                tempArray = new int[2];
                tempArray[0] = 0;
                tempArray[1] = 1;
                vectorMap.put(tch, tempArray);
            }

        }

    }

    // 求余弦相似度
    public double sim() {
        double result = 0;
        result = pointMulti(vectorMap) / sqrtMulti(vectorMap);
        return result;
    }

    // 求平方和
    private double squares(Map<Character, int[]> paramMap) {
        double result1 = 0;
        double result2 = 0;
        Set<Character> keySet = paramMap.keySet();
        for (Character character : keySet) {
            int temp[] = paramMap.get(character);
            result1 += (temp[0] * temp[0]);
            result2 += (temp[1] * temp[1]);
        }
        return result1 * result2;
    }

    // 点乘法
    private double pointMulti(Map<Character, int[]> paramMap) {
        double result = 0;
        Set<Character> keySet = paramMap.keySet();
        for (Character character : keySet) {
            int temp[] = paramMap.get(character);
            result += (temp[0] * temp[1]);
        }
        return result;
    }

    private double sqrtMulti(Map<Character, int[]> paramMap) {
        double result = 0;
        result = squares(paramMap);
        result = Math.sqrt(result);
        return result;
    }

    public static void main(String[] args) {

        String s1="食品保健零食坚果特产糕点点心西式糕点面包";
        String s2="生鲜面点烘焙面点";
        String s3="服饰内衣女装连衣裙";
        String s4="服饰箱包女装女士竞品连衣裙连衣裙";

        TTT t1 =new TTT(s4,s1);
        TTT t2 =new TTT(s4,s2);
        TTT t3 =new TTT(s4,s3);

        TTT t4 =new TTT(s1,s2);
        TTT t5 =new TTT(s1,s3);
        TTT t6 =new TTT(s1,s4);

        System.out.println("==s4与s1相似度==="+t1.sim());
        System.out.println("==s4与s2相似度==="+t2.sim());
        System.out.println("==s4与s3相似度==="+t3.sim());
        System.out.println("--------------------------------------------------");
        System.out.println("==s1与s2相似度==="+t4.sim());
        System.out.println("==s1与s3相似度==="+t5.sim());
        System.out.println("==s1与s4相似度==="+t6.sim());

    }
}
