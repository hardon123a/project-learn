package com.learn.se;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class OrderNum {

    public static void main(String[] args) {
        /*NumberFormat f=new DecimalFormat("00000");
        for(int i=1;i<10000;i++){
            System.out.println(f.format(i));
        }*/

        /*String m = "ER-";
        for (int i = 0; i < 10000; i++) {
            String format = String.format("%05d", i);
            System.out.println(m+format);
        }*/

        // 00001-99999
        /*for (int i = 9; i < 100005; i++) {
            System.out.println(String.format("%05d", i));
        }*/
        // 如果后5位等于99999，
        // 则A0001-A9999
        // 如果后4位等于9999，且倒数第5位不是数字，则打印出该字母的下一下字母
        // 如果后5位数字是Z9999，则提示已达到编号的最大值


        System.out.println("数据开始生成");
        String baseCode="BLA3202X";

        String maxCode="BLA3202XY0001";
        for(int i=0;i<10000000;i++){

            String codeSuffix=maxCode.substring(8);
            // 如果索引第0位是字母，则从初始编码索引第9位取
            if(Character.isUpperCase(codeSuffix.substring(0,1).charAt(0))){
                codeSuffix=maxCode.substring(9);
            }
            // 获取数字
            String codeNum = codeSuffix.replaceFirst("^0*", "");
            //System.out.println(codeNum);

            // 转换成Int类型的数字
            Integer intCodeNum=Integer.valueOf(codeNum)+1;

            // 定义为产生的新编码
            String newCode="";
            if(intCodeNum>99999){
                newCode="BLA3202XA0001";
            }else{
                // 获取第9位数字
                String flag=maxCode.substring(8,9);
                if(Character.isUpperCase(flag.charAt(0))){
                    if(intCodeNum>9999){
                        newCode=baseCode+getNextUpEn(flag)+"0001";

                    }else{
                        String codeNumStr=String.format("%04d", intCodeNum);
                        newCode=baseCode+flag+codeNumStr;
                    }
                }else{
                    String codeNumStr=String.format("%05d", intCodeNum);
                    newCode=baseCode+codeNumStr;
                }
            }
            System.out.println(newCode);
            maxCode=newCode;

            if(newCode.equals("BLA3202XZ9999")){
                break;
            }
        }

        // Integer num=200;
        // 00001-99999
        /*for (int i = 1; i < num; i++) {
            System.out.println(String.format("%05d", i));
            String suffix=String.format("%05d", i);
            if(suffix)
        }*/

        //System.out.println(getNextUpEn("A"));
        //String str="A";
        //System.out.println(Character.isUpperCase(str.charAt(0)));

    }

    public static String getNextUpEn(String en){
        char lastE = 'a';
        char st = en.toCharArray()[0];
        if(Character.isUpperCase(st)){
            if(en.equals("Z")){
                return "A";
            }
            if(en==null || en.equals("")){
                return "A";
            }
            lastE = 'Z';
        }else{
            if(en.equals("z")){
                return "a";
            }
            if(en==null || en.equals("")){
                return "a";
            }
            lastE = 'z';
        }
        int lastEnglish = (int)lastE;
        char[] c = en.toCharArray();
        if(c.length>1){
            return null;
        }else{
            int now = (int)c[0];
            if(now >= lastEnglish)
                return null;
            char uppercase = (char)(now+1);
            return String.valueOf(uppercase);
        }
    }
}
