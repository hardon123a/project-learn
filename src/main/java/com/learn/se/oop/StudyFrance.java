package com.learn.se.oop;

public interface StudyFrance extends StudyForeign {
    @Override
    void studySpoken();

    @Override
    void studyWritten();
}
