package com.learn.se.oop;

public interface StudyGerman extends StudyForeign {

    @Override
    void studySpoken();

    @Override
    void studyWritten();
}
