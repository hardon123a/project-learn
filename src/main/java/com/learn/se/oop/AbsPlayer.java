package com.learn.se.oop;

public abstract class AbsPlayer extends AbsPerson{

    public AbsPlayer() {
    }

    public AbsPlayer(String name, int age) {
        super(name, age);
    }

    public abstract void study();
}
