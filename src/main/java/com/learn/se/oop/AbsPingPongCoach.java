package com.learn.se.oop;

public abstract class AbsPingPongCoach extends AbsCoach{

    public AbsPingPongCoach() {
    }

    public AbsPingPongCoach(String name, int age) {
        super(name, age);
    }
}
