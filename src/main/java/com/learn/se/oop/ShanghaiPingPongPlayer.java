package com.learn.se.oop;

public class ShanghaiPingPongPlayer extends AbsPingPongPlayer implements StudyFrance {
    @Override
    public void study() {
        System.out.println("上海乒乓球运动员开始训练了");
    }

    @Override
    public void eat() {
        System.out.println("上海乒乓球运动员吃火锅，喝王老吉");
    }

    @Override
    public void studySpoken() {
        System.out.println("上海篮乒乓球运动员学习英语口语");
    }

    @Override
    public void studyWritten() {
        System.out.println("上海篮乒乓球运动员学习英语书面表达");
    }
}
