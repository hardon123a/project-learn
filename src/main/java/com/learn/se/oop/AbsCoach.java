package com.learn.se.oop;

public abstract class AbsCoach extends AbsPerson{

    public AbsCoach() {
    }

    public AbsCoach(String name, int age) {
        super(name, age);
    }

    public abstract void teach();

}
