package com.learn.se.oop;

public abstract class AbsBasketballPlayer extends AbsPlayer{

    public AbsBasketballPlayer() {
    }

    public AbsBasketballPlayer(String name, int age) {
        super(name, age);
    }
}
