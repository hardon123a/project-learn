package com.learn.se.oop;

public abstract class AbsBasketballCoach extends AbsCoach{

    public AbsBasketballCoach() {
    }

    public AbsBasketballCoach(String name, int age) {
        super(name, age);
    }
}
