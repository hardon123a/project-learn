package com.learn.se.oop;

public class ShanghaiBasketballPlayer extends AbsBasketballPlayer implements StudyEnglish{

    public ShanghaiBasketballPlayer() {
        super();
    }

    public ShanghaiBasketballPlayer(String name, int age) {
        super(name, age);
    }

    @Override
    public void study() {
        System.out.println("上海篮球运动员开始训练了");
    }

    @Override
    public void eat() {
        System.out.println("上海篮球运动员吃牛肉，喝啤酒");
    }

    @Override
    public void studySpoken() {
        System.out.println("上海篮球运动员学习英语口语");
    }

    @Override
    public void studyWritten() {
        System.out.println("上海篮球运动员学习英语书面表达");
    }
}
