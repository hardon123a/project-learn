package com.learn.se.oop;

public interface StudyForeign {

    /**
     * 口语学习
     */
    public abstract void studySpoken();

    /**
     * 书面表达
     */
    public abstract void studyWritten();


}
