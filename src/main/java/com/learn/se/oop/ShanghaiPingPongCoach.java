package com.learn.se.oop;

public class ShanghaiPingPongCoach extends AbsPingPongCoach {
    public ShanghaiPingPongCoach() {
        super();
    }

    public ShanghaiPingPongCoach(String name, int age) {
        super(name, age);
    }

    @Override
    public void eat() {
        System.out.println("上海乒乓教练吃羊肉，喝啤酒");
    }

    @Override
    public void teach() {
        System.out.println("上海乒乓教练开始训练了");
    }
}
