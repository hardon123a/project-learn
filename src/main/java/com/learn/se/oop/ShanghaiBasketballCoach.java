package com.learn.se.oop;

public class ShanghaiBasketballCoach extends AbsBasketballPlayer{
    public ShanghaiBasketballCoach() {
        super();
    }

    public ShanghaiBasketballCoach(String name, int age) {
        super(name, age);
    }

    @Override
    public void study() {

        System.out.println("上海篮球教练开始训练了");
    }

    @Override
    public void eat() {
        System.out.println("上海篮球教练吃羊肉喝红酒");
    }

}
