package com.learn.se.oop;

public interface StudyEnglish extends StudyForeign {

    @Override
    void studySpoken();

    @Override
    void studyWritten();
}
