package com.learn.se.oop;

public abstract class AbsPingPongPlayer extends AbsPlayer{

    public AbsPingPongPlayer() {
    }

    public AbsPingPongPlayer(String name, int age) {
        super(name, age);
    }
}
