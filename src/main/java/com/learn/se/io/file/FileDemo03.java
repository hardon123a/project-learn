package com.learn.se.io.file;

import java.io.File;

public class FileDemo03 {

    public static void main(String[] args) {

        //创建一个File对象
        File file=new File("D:/course/VIP专享--2020期录播视频及资料/2021/spring/4/01.用300行代码手写初体验Spring%20V1.0版本/01.课堂源码/gupaoedu-vip-spring-1.0/target/classes/com/gupaoedu/demo");
        //File file=new File("D:/filetest");
        // public boolean isDirectory()：测试此抽象路径名表示的File是否为目录
// public boolean isFile()：测试此抽象路径名表示的File是否为文件
// public boolean exists()：测试此抽象路径名表示的File是否存在
        System.out.println(file.isDirectory());
        System.out.println(file.isFile());
        System.out.println(file.exists());
// public String getAbsolutePath()：返回此抽象路径名的绝对路径名字符串
// public String getPath()：将此抽象路径名转换为路径名字符串
// public String getName()：返回由此抽象路径名表示的文件或目录的名称
        System.out.println(file.getAbsolutePath());
        System.out.println(file.getPath());
        System.out.println(file.getName());
        System.out.println("----------------------------");

        //返回此抽象路径名表示的目录中的文件和目录的名称字符串数
        /*String[] strArray=file.list();
        for (String str:strArray) {
            System.out.println(str);
        }
        System.out.println("----------------------------");*/

        //返回此抽象路径名表示的目录中的文件和目录的File对象数组
        System.out.println("-------------测试开始---------------");
        File[] fileArray=file.listFiles();
        for(File f:fileArray){
            if(f.isDirectory()){
                System.out.println("这个目录--"+f.getName());
            }
            if(f.isFile()){
                System.out.println("这个文件--"+f.getName());
            }
        }

    }

}
