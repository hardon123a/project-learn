package com.learn.se.io.file;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileInputStreamDemo01 {
    public static void main(String[] args) throws IOException {
        //创建字节输入流对象
        FileInputStream fis=new FileInputStream("D://filetest//b.txt");
        int by;
        /*
        1.fis.read()：读数据
        2.by=fis.read()：把读取到的数据赋值给by
        3.by != -1：判断读取到的数据是否是-1
         */
        while ((by=fis.read())!=-1){
            System.out.print((char)by);
        }
        //释放资源
        fis.close();
    }
}
