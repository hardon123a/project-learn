package com.learn.se.io.file;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyJpgDemo {

    public static void main(String[] args) throws IOException {

        //根据数据源创建字节输入流对象
        FileInputStream fis = new FileInputStream("D:\\filetest\\1.jpg");
        //根据目的地创建字节输出流对象
        FileOutputStream fos = new FileOutputStream("D:\\filetest\\2.jpg");

        //读写数据，复制文本文件(一次读取一个字节，一次写入一个字节)
        byte[] bys=new byte[1024];//1024及其整数倍
        int len;
        while ((len=fis.read(bys))!=-1){
            fos.write(bys);
        }

        //释放资源
        fos.close();
        fis.close();
    }
}
