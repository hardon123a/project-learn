package com.learn.se.io.file;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileOutputStreamDemo01 {

    public static void main(String[] args) throws IOException {
        //创建字节输出流对象
        FileOutputStream fos = new FileOutputStream("D://filetest/fos.txt");
        /*
          做了三件事情：
          A:调用系统功能创建了文件
          B:创建了字节输出流对象
          C:让字节输出流对象指向创建好的文件
        */
        //将指定的字节写入此文件输出流
        //fos.write(98);
        byte[] bys = {97, 98, 99, 100, 101};
        //fos.write(bys);
        //fos.write("ABCDE".getBytes());
        fos.write(bys,1,3);
        //要释放资源,关闭此文件输出流并释放与此流相关联的任何系统资源
        fos.close();


    }
}
