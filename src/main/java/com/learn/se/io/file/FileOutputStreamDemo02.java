package com.learn.se.io.file;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class FileOutputStreamDemo02 {

    public static void main(String[] args) throws IOException {
        FileOutputStream fos=new FileOutputStream("D://filetest//b.txt");
        fos.write("hello".getBytes());
        fos.write("\r\n".getBytes());
        fos.write("hello".getBytes());
        fos.close();

    }
}
