package com.learn.se.io.file;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/*
    字节缓冲流：
    BufferOutputStream
    BufferedInputStream

    构造方法：
    字节缓冲输出流：BufferedOutputStream​(OutputStream out)
    字节缓冲输入流：BufferedInputStream​(InputStream in)
*/
public class BufferStreamDemo {

    public static void main(String[] args) throws IOException {

        //字节缓冲输入流：BufferedInputStream​(InputStream in)
        BufferedInputStream bis=new BufferedInputStream(new FileInputStream("D://filetest//java.txt"));

        //一次读取一个字节数据
        int by;
        while ((by=bis.read())!=-1) {
            System.out.print((char)by);
        }

        System.out.println();
        System.out.println("----------------------------------");

        //一次读取一个字节数组数据
        /*byte[] bys=new byte[1024];//1024及其整数倍
        int len;
        while ((len=bis.read(bys))!=-1){
            System.out.println(new String(bys,0,len));
        }*/

        //释放资源
     }
}
