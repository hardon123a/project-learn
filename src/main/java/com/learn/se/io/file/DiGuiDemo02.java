package com.learn.se.io.file;

import java.io.File;

public class DiGuiDemo02 {

    public static void main(String[] args) {
        File file =new File("D://filetest");
        getAllFileFath(file);
    }

    //定义一个方法，用于获取给定目录下的所有内容，参数为第1步创建的File对象
    public static void getAllFileFath(File srcFile){
        //获取给定文件下的文件或文件目录
        File[] fileArray=srcFile.listFiles();
        //遍历File数组，得到每一个File对象
        if(fileArray!=null){
            for(File file:fileArray){
                //判断该File对象是否是目录
                if(file.isDirectory()){
                    //是，调用递归
                    getAllFileFath(file);
                }else {
                    //不是，获取绝对路径并输出到控制台
                    System.out.println(file.getPath());
                }
            }
        }
    }
}
