package com.learn.se.io.file;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;

/**
 * File类创建功能：
 *         public boolean createNewFile()：当具有该名称的文件不存在时，创建一个由该抽象路径名命名的新空文件
 *             如果文件不存在，就创建文件，并返回true
 *             如果文件存在，就不创建文件，并返回false
 *
 *         public boolean mkdir()：创建由此抽象路径名命名的目录
 *             如果目录不存在，就创建目录，并返回true
 *             如果目录存在，就不创建目录，并返回false
 *
 *         public boolean mkdirs()：创建由此抽象路径名命名的目录，包括任何必需但不存在的父目录
 */
public class FileDemo02 {

    public static void main(String[] args) throws IOException {

        //需求1：我要在D:\\filetest目录下创建一个文件java.txt
        File f1=new File("D:\\filetest\\java.txt");
        System.out.println(f1.createNewFile());

        //需求2：我要在D:\\filetest目录下创建一个目录JavaSE
        File f2=new File("D:\\filetest\\JavaSE");
        System.out.println(f2.mkdir());

        //需求3：我要在D:\\filetest目录下创建一个多级目录JavaWEB\\HTML
        File f3=new File("D:\\filetest\\JavaWEB\\HTML");
        System.out.println(f3.mkdirs());

        //需求4：我要在D:\\filetest目录下创建一个文件javase.txt
        File f4=new File("D:\\filetest\\javase.txt");
        System.out.println(f4.createNewFile());


        //需求5：我要在D:\\filetest目录下删除文件javase.txt
        File f5=new File("D:\\filetest\\javase.txt");
        System.out.println(f5.delete());
    }

}
