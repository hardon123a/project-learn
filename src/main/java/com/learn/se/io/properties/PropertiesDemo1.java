package com.learn.se.io.properties;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class PropertiesDemo1 {

    public static void main(String[] args) throws IOException {
        //把集合中的数据保存到文件
        myLoad();
        //把文件中的数据加载到集合
        //myStore();
    }

    public static void myLoad() throws IOException {

        Properties pros=new Properties();
        FileReader fr=new FileReader("D://filetest//e.txt");
        pros.load(fr);
        fr.close();
        System.out.println(pros);
    }

    public static void myStore() throws IOException {
        Properties prop = new Properties();
        prop.setProperty("001","北京");
        prop.setProperty("002","上海");
        prop.setProperty("003","广州");
        prop.setProperty("004","深圳");

        FileWriter fr=new FileWriter("D://filetest//e.txt");
        prop.store(fr,null);

        fr.close();
    }
}
