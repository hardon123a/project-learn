package com.learn.se.extend;

import java.util.concurrent.*;

public class FutureTest {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        ExecutorService service = Executors.newSingleThreadExecutor();
        Future<String> future = service.submit(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return "say helloWorld!!!";
            }
        });
        System.out.println(future.get());// 通过get返回结果


        ExecutorService service2 = Executors.newSingleThreadExecutor();
        FutureTask<String> futureTask = new FutureTask<>(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return "futureTask say HelloWorld!!!";
            }
        });
        service2.execute(futureTask);
        System.out.println(futureTask.get());
    }
}
