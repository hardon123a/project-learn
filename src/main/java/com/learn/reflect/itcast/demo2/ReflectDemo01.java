package com.learn.reflect.itcast.demo2;

import com.learn.reflect.itcast.demo1.Student;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/*
    反射获取构造方法并使用
 */
public class ReflectDemo01 {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        //获取class对象
        Class<?> c = Class.forName("com.learn.reflect.itcast.demo1.Student");
        Constructor<?>[] cons = c.getDeclaredConstructors();
        for(Constructor con:cons){
            System.out.println(con);
        }
        System.out.println("--------");

        Constructor<?> con = c.getConstructor();
        //Constructor提供了一个类的单个构造函数的信息和访问权限
        //T newInstance​(Object... initargs) 使用由此 Constructor对象表示的构造函数，使用指定的初始化参数来创建和初始化构造函数的声明类的新实例Ob
        Object obj=con.newInstance();

        System.out.println(obj);

        Student s = new Student();
        System.out.println(s);

    }
}
