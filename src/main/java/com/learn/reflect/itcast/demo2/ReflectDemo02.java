package com.learn.reflect.itcast.demo2;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 *通过反射实现如下的操作：
 *         Student s = new Student("林青霞",30,"杭州");
 *         System.out.println(s);
 */
public class ReflectDemo02 {

    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        //获取Class对象
        Class<?> c = Class.forName("com.learn.reflect.itcast.demo1.Student");
        Constructor<?> con = c.getConstructor(String.class, int.class, String.class);
        //基本数据类型也可以通过.class得到对应的Class类型
        Object obj=con.newInstance("林青霞",30,"杭州");

        System.out.println(obj);
    }
}
