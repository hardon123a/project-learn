package com.learn.reflect.itcast.demo4;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectDemo02 {

    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        //获取Class对象
        Class<?> c = Class.forName("com.learn.reflect.itcast.demo1.Student");

        //Student s = new Student();
        Constructor<?> con = c.getConstructor();
        Object obj=con.newInstance();

        //s.method1();
        Method method1=c.getMethod("method1");
        method1.invoke(obj);

        //s.method2("林青霞");
        Method method2=c.getMethod("method2",String.class);
        method2.invoke(obj,"Huang");

        //s.method3("林青霞",30);
        Method method3=c.getMethod("method3",String.class,int.class);
        method3.invoke(obj,"Huang",30);

        //s.function();
        Method method4=c.getDeclaredMethod("function");
        method4.setAccessible(true);
        method4.invoke(obj);


    }
}
