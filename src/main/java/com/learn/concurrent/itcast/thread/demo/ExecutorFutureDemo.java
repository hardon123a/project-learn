package com.learn.concurrent.itcast.thread.demo;

import java.util.concurrent.*;

public class ExecutorFutureDemo implements Callable<Integer> {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        long startTime = System.currentTimeMillis();
        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                1,
                1,
                0L,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<>());

        Future<Integer> result = executor.submit(new ExecutorFutureDemo());

        System.out.println("开始查询服务器 B ...");
        // 保存服务器 B 查询结果
        int sum = 0;
        // 主线程查询服务器 B 的结果
        for (int i = 1; i <= 100 ; i++) {
            sum += i;
        }
        // 模拟服务器 B 的查询时间
        Thread.sleep(3000);
        System.out.println("服务器 B 查询完毕。");
        if(!result.isDone()){
            System.out.println("服务器 A 数数据还没有查询完毕...");
        }
        // 在没有得到结果之前会一直阻塞在这里，直到拿到返回结果
        Integer aSum = result.get();
        long endTime = System.currentTimeMillis();
        System.out.println("查询结果="+(aSum+sum)+",总耗时="+(endTime-startTime)/1000);
        // 关闭线程池
        executor.shutdown();
    }

    @Override
    public Integer call() throws Exception {
        System.out.println("开始查询服务器 A ...");
        // 保存服务器 A 查询结果
        int sum = 0;
        try {
            // 模拟服务器 A 的 查询时间
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (int i = 1; i <= 50 ; i++) {
            sum += i;
        }
        System.out.println("服务器 A 查询完毕。");
        return sum;
    }
}