package com.learn.concurrent.itcast.thread.queue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class ProducerAndConsumer {

    public static void main(String[] args) {
        BlockingQueue queue = new LinkedBlockingQueue(5);
        ExecutorService executor = Executors.newFixedThreadPool(5);

        Producer producer = new Producer(queue);
        for (int i = 0; i < 3; i++) {
            executor.execute(producer);
        }

        executor.execute(new Consumer(queue));

        executor.shutdown();
    }
}
