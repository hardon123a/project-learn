package com.learn.concurrent.itcast.thread.interaction.example3;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Case2 {

    public static void main(String[] args) {
        ExecutorService pool = Executors.newFixedThreadPool(3);

        pool.execute(() -> {
            try {
                System.out.println(Thread.currentThread() + "执行任务。。。。。。");
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        pool.execute(() -> {
            try {
                System.out.println(Thread.currentThread()+ "执行任务。。。。。。");
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        //不在接受新任务
        pool.shutdown();

        while (true) {
            //手动循环确实效率很低，不推荐
            if (pool.isTerminated()) {
                System.out.println("线程池中的任务执行结束");
                break;
            }
        }

        System.out.println("主线程结束");
    }
}
