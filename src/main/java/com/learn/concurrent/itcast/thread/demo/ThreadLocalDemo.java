package com.learn.concurrent.itcast.thread.demo;

public class ThreadLocalDemo {
    //1.创建银行对象：钱，取款，存款
    static class Bank{
        private ThreadLocal<Integer> threadLocal=new ThreadLocal<Integer>(){
            @Override
            protected Integer initialValue() {
                return 0;
            }
        };

        public Integer get(){
            return threadLocal.get();
        }

        public void set(Integer money){
            threadLocal.set(threadLocal.get()+money);
        }

    }
    //2.创建转账对象：从银行中取钱，转账，保存到帐户
    static class Transfer implements Runnable{

        private Bank bank;

        public Transfer(Bank bank) {
            this.bank = bank;
        }

        @Override
        public void run() {
            for (int i = 0; i <10 ; i++) {
                bank.set(10);
                System.out.println("向"+Thread.currentThread().getName()+"转账 10");
                System.out.println(Thread.currentThread().getName()+"账号余额："+bank.threadLocal.get());
            }
        }
    }
    //3.在main方法中使用两个对象模拟转账
    public static void main(String[] args) {
        Bank bank=new Bank();
        Transfer transfer=new Transfer(bank);
        Thread thread1=new Thread(transfer,"客户一");
        Thread thread2=new Thread(transfer,"客户二");
        thread1.start();
        thread2.start();
    }
}
