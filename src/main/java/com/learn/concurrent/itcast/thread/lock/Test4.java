package com.learn.concurrent.itcast.thread.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Test4 {
    public static void main(String[] args) {
        Lock lock=new ReentrantLock();
        lock.lock();
        try {
            System.out.println("获得锁");
        } finally {
            System.out.println("释放锁");
            lock.unlock();
        }
    }
}
