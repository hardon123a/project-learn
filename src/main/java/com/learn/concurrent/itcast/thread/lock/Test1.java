package com.learn.concurrent.itcast.thread.lock;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * https://www.jianshu.com/p/9cd5212c8841
 * 一个线程获取多少次锁，就必须释放多少次锁。这对于内置锁也是适用的，每一次进入和离开synchornized方法(代码块)，
 * 就是一次完整的锁获取和释放。
 */
public class Test1 {

    public static void main(String[] args) throws InterruptedException {
        ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
        Thread t=new Thread(new Runnable() {
            @Override
            public void run() {
                lock.writeLock().lock();
                System.out.println("Thread real execute");
                lock.writeLock().unlock();
            }
        });

        lock.writeLock().lock();
        lock.writeLock().unlock();
        t.start();
        Thread.sleep(2000);

        System.out.println("realse one once");
        lock.writeLock().unlock();

    }
}
