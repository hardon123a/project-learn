package com.learn.concurrent.itcast.thread.interaction.example3;

/**
 * 如何让主线程等待所有的子线程结束之后再执行
 */
public class Case1 {
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(() -> {
            try {
                System.out.println(Thread.currentThread()+"执行任务开始。。。。");
                Thread.sleep(3000);
                System.out.println(Thread.currentThread()+"执行任务结束。。。。");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }, "线程一");
        t1.start();

        Thread t2 = new Thread(() -> {
            try {
                System.out.println(Thread.currentThread()+"执行任务开始。。。。");
                Thread.sleep(3000);
                System.out.println(Thread.currentThread()+"执行任务结束。。。。");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }, "线程二");
        t2.start();


        Thread t3 = new Thread(() -> {
            try {
                System.out.println(Thread.currentThread()+"执行任务开始。。。。");
                Thread.sleep(3000);
                System.out.println(Thread.currentThread()+"执行任务结束。。。。");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }, "线程三");
        t3.start();

        t1.join();
        //t2.join();
        //t3.join();
        System.out.println("主线程结束");
    }
}
