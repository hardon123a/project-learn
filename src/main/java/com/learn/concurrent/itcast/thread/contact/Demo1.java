package com.learn.concurrent.itcast.thread.contact;

public class Demo1 {

    /**
     * https://www.cnblogs.com/wenjunwei/p/1057328
     */
    private static volatile int state=0;

    public static void main(String[] args) throws InterruptedException {
        /*
        客户、商家、配送
        客户： 叫外卖下订单，支付，取餐、开始吃饭
        商家： 确认订单、开发做外卖
        配送人员 接单，配送快餐，配送人员把外卖放到规定存放位置

         */
        Thread thread1=new Thread(new Runnable() {
            @Override
            public void run() {
                if(state==0) {
                    System.out.println(Thread.currentThread() + " 用户叫外卖下订单");
                }
                state++;
            }
        },"用户线程");

        Thread thread2=new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread()+" 确认订单、开发做外卖");
            }
        },"商家线程");

        Thread thread3=new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread()+" 接单，配送快餐，配送人员把外卖放到规定存放位置");
            }
        },"配送线程");

        thread1.start();
        thread2.start();
        //thread2.start();
        thread3.start();
        thread1.join();
        System.out.println("外卖流程演示结束。。。");

    }
}
