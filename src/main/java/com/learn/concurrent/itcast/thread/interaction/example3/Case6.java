package com.learn.concurrent.itcast.thread.interaction.example3;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * 等到两个子任务都完成后，输出两数之积，再执行主线程
 * https://www.jianshu.com/p/ba8a518c02c3
 */
public class Case6 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        CompletableFuture<Integer> cf1 = CompletableFuture.supplyAsync(() -> {
            try {

                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 2;
        });

        CompletableFuture<Integer> cf = CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 3;
        }).thenCombine(cf1, (result1, result2) -> result1 * result2);

        //get方法为阻塞获取
        System.out.println("计算结果为" + cf.get());
        System.out.println("主线程结束");

    }
}
