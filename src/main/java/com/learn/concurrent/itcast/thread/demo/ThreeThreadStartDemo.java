package com.learn.concurrent.itcast.thread.demo;

import java.util.Date;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class ThreeThreadStartDemo {

    private CyclicBarrier cyclicBarrier=new CyclicBarrier(3);


    public void startThread(){
        //1.打印线程准备启动
        String name=Thread.currentThread().getName();
        System.out.println(name+"正在准备。。。");
        //2.调用CyclicBarrier的await 方法等待全部线程准备完成
        try {
            cyclicBarrier.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
        //3.打印线程启动完毕信息
        System.out.println(name+"准备完毕，起跑："+new Date().getTime());
    }

    public static void main(String[] args) {

        final ThreeThreadStartDemo threeThreadStartDemo=new ThreeThreadStartDemo();
        new Thread(() -> {
            threeThreadStartDemo.startThread();
        },"选手1").start();

        new Thread(() -> {
            threeThreadStartDemo.startThread();
        },"选手2").start();

        new Thread(() -> {
            threeThreadStartDemo.startThread();
        },"选手3").start();
    }
}
