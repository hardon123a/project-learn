package com.learn.concurrent.itcast.thread.interaction.example3;

import java.util.concurrent.CountDownLatch;

public class CountDownlatchTest {
    public static void main(String[] args) throws InterruptedException {

        CountDownLatch countDownLatch=new CountDownLatch(6);
        for(int i=0;i<6;i++) {
            Thread thread = new Thread(new ReadNum(i, countDownLatch));
            thread.start();
        }
        countDownLatch.await();
        System.out.println("线程执行结束。。。。");

    }
    static class ReadNum implements Runnable{

        private int id;
        private CountDownLatch latch;

        public ReadNum(int id, CountDownLatch latch) {
            this.id = id;
            this.latch = latch;
        }

        @Override
        public void run() {
            synchronized (this){
                System.out.println("任务ID："+id);
                System.out.println("线程组任务"+id+"结束，其他任务继续");
                latch.countDown();

            }

        }
    }
}
