package com.learn.concurrent.itcast.thread.queue;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {

    private BlockingQueue queue;

    private int nums = 20; //循环次数

    private boolean isRunning = true;

    public Consumer() {
    }

    public Consumer(BlockingQueue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        System.out.println("消费者开始消费");
        System.out.println("-------------------------");

        while (nums > 0) {
            nums--;
            try {
                while (isRunning) {
                    int data = (Integer) queue.take();
                    Thread.sleep(500);
                    System.out.println("消费者消费的数据是" + data);
                }

            } catch (Exception e) {
                e.printStackTrace();
                Thread.currentThread().interrupt();
            } finally {
                System.out.println("消费者线程退出!");
            }
        }
    }
}
