package com.learn.concurrent.itcast.thread.safe;

public class TicketSaleMain {

    public static void main(String[] args) {

        //1.创建电影票对象
        Ticket ticket=new Ticket();
        TicketLock ticketLock=new TicketLock();

        //2.创建Tread对象，执行电影票售卖
        /*Thread thread1=new Thread(ticket,"窗口1");
        Thread thread2=new Thread(ticket,"窗口2");
        Thread thread3=new Thread(ticket,"窗口3");

        thread1.start();
        thread2.start();
        thread3.start();*/
        //3.lock
        Thread thread1=new Thread(ticketLock,"窗口1");
        Thread thread2=new Thread(ticketLock,"窗口2");
        Thread thread3=new Thread(ticketLock,"窗口3");

        thread1.start();
        thread2.start();
        thread3.start();
    }
}
