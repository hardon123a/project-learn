package com.learn.concurrent.itcast.thread.queue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Producer implements Runnable{

    private BlockingQueue queue;

    private int nums=20; //循环次数

    //标记数据编号
    private static volatile AtomicInteger count=new AtomicInteger();
    private boolean isRunning=true;

    public Producer(BlockingQueue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        String data=null;
        try{
            System.out.println("开始生产数据");
            System.out.println("-----------------------");
            while (nums>0){
                nums--;
                count.decrementAndGet();

                Thread.sleep(500);
                System.out.println(Thread.currentThread().getId()+ " :生产者生产了一个数据");
                queue.put(count.getAndIncrement());
            }
        }catch (Exception e){
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }finally {
            System.out.println("生产者线程退出！");
        }
    }
}
