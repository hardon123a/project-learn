package com.learn.concurrent.itcast.thread.create;

import java.util.Date;
import java.util.concurrent.Callable;

public class MyCallable implements Callable<String> {

    @Override
    public String call() throws Exception {
        for(int i=0;i<10;i++){
            System.out.println("MyCallable线程正在执行时间："+new Date().getTime()+"执行次数"+i);
        }
        return "MyCallable执行完毕！";
    }
}
