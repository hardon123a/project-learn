package com.learn.concurrent.itcast.thread.join;

public class Test {

    static class Girl implements Runnable{
        @Override
        public void run() {
            System.out.println("Girl开始排队打饭。。。");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getId()+" Girl打饭完成");
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Girl girl=new Girl();
        Thread g=new Thread(girl);
        g.start();

        System.out.println((Thread.currentThread().getId()+" 小亮开始排队打饭..."));
        g.join();
        Thread.sleep(1000);
        System.out.println((Thread.currentThread().getId()+" 小亮打饭完成..."));

    }
}
