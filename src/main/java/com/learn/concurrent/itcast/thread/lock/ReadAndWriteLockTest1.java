package com.learn.concurrent.itcast.thread.lock;

public class ReadAndWriteLockTest1 {

    public synchronized static void get(Thread thread){
        System.out.println("start time:"+System.currentTimeMillis());

        for (int i = 0; i < 6; i++) {
            try {
                Thread.sleep(200);
                System.out.println(thread.getName()+"正在进行读操作");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(thread.getName()+"读操作完毕");
        System.out.println("end time"+System.currentTimeMillis());
    }

    public static void main(String[] args) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                get(Thread.currentThread());
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                get(Thread.currentThread());
            }
        }).start();
    }
}
