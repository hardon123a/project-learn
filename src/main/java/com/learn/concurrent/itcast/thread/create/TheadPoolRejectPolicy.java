package com.learn.concurrent.itcast.thread.create;

import java.util.concurrent.*;
import java.util.logging.Logger;

/**
 *
 * https://blog.csdn.net/alan_liuyue/article/details/120995601
 */
public class TheadPoolRejectPolicy {

    private static final Logger log = Logger.getLogger(String.valueOf(TheadPoolRejectPolicy.class));

    public static void main(String[] args) throws Exception{
        int corePoolSize = 5;
        int maximumPoolSize = 10;
        long keepAliveTime = 5;
        BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<Runnable>(10);
        RejectedExecutionHandler handler = new ThreadPoolExecutor.DiscardPolicy();
        ThreadPoolExecutor executor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, TimeUnit.SECONDS, workQueue, handler);
        for(int i=0; i<100; i++) {
            try {
                executor.execute(new Thread(() -> log.info(Thread.currentThread().getName() + " is running")));
            } catch (Exception e) {
                System.out.println(e);
            }
        }
        executor.shutdown();
    }
}
