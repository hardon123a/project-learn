package com.learn.concurrent.itcast.thread.safe;

import java.util.concurrent.atomic.AtomicInteger;

public class VolatileTest {

    public static void main(String[] args) {
        Thread thread1=new Thread(new Part());
        Thread thread2=new Thread(new Part());
        thread1.start();
        thread2.start();

    }
}

class Part implements Runnable{

    //private volatile static int a=0;
    private volatile static AtomicInteger a=new AtomicInteger(0);

    @Override
    public void run() {

        for (int i = 0; i <10 ; i++) {
            System.out.println(Thread.currentThread().getName()+" a="+a.incrementAndGet());
            //System.out.println(Thread.currentThread().getName()+" a="+a);
            //a++;
        }
    }
}

