package com.learn.concurrent.itcast.thread.interaction.example3;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Case4 {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService pool = Executors.newFixedThreadPool(5);

        Future<Integer> task1 = pool.submit(() -> {
            try {
                System.out.println(Thread.currentThread() + "执行任务。。。。。。");
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 2;
        });

        Future<Integer> task2 = pool.submit(() -> {
            try {
                System.out.println(Thread.currentThread() + "执行任务。。。。。。");
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 3;
        });

        //不在接受新任务
        pool.shutdown();

        //get方法为阻塞获取
        System.out.println("task1的运行结果:" + task1.get());
        System.out.println("task2的运行结果:" + task2.get());

        System.out.println("task1、task2 运行结果:" + task1.get().intValue()+task2.get().intValue());

        System.out.println("主线程结束");
    }
}
