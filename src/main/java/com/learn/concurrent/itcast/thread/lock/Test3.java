package com.learn.concurrent.itcast.thread.lock;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 结论：ReentrantReadWriteLock支持锁降级。
 * 从写锁降级成读锁，并不会自动释放当前线程获取的写锁，仍然需要显示的释放，否则别的线程永远也获取不到写锁。
 */
public class Test3 {
    public static void main(String[] args) {
        ReentrantReadWriteLock rtLock=new ReentrantReadWriteLock();
        rtLock.writeLock().lock();
        System.out.println("get write lock");
        rtLock.readLock().lock();
        System.out.println("get read lock");
    }
}
