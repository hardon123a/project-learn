package com.learn.concurrent.threadlocal;

public class MyDemo1 {

    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public static void main(String[] args) {
        MyDemo1 demo=new MyDemo1();
        for(int i=0;i<5;i++){
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    demo.setContent(Thread.currentThread().getName() + "的数据");
                    System.out.println(Thread.currentThread().getName() + "--->" + demo.getContent());
                    System.out.println("-----------------------");

                }
            });
            thread.setName("线程" + i);
            thread.start();

        }
    }
}
