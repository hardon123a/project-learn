package com.learn.concurrent.lock;

/**
 * https://www.cnblogs.com/weibanggang/p/9470718.html
 * synchronized是Java中的关键字，是一种同步锁。它修饰的对象有以下几种：
 * 　　1. 修饰一个代码块，被修饰的代码块称为同步语句块，其作用的范围是大括号{}括起来的代码，作用的对象是调用这个代码块的对象；
 * 　　2. 修饰一个方法，被修饰的方法称为同步方法，其作用的范围是整个方法，作用的对象是调用这个方法的对象；
 * 　　3. 修改一个静态的方法，其作用的范围是整个静态方法，作用的对象是这个类的所有对象；
 * 　　4. 修改一个类，其作用的范围是synchronized后面括号括起来的部分，作用主的对象是这个类的所有对象。
 */
public class Synchronized_04 {
    public static void main(String[] args) {
        System.out.println("使用ClassName");
        SyncThread04 syncThread = new SyncThread04();
        Thread thread1 = new Thread(syncThread, "SyncThread1");
        //Thread thread2 = new Thread(syncThread, "SyncThread2");
        Thread thread2 = new Thread(new SyncThread04(), "SyncThread2");
        thread1.start();
        thread2.start();
    }
}


class ClassName {
    public void method() {
        synchronized(Synchronized_04.class) {

        }
    }
}
class SyncThread04 implements Runnable {
    private static int count;

    public SyncThread04() {
        count = 0;
    }

    public static void method() {
        synchronized(Synchronized_04.class) {
            for (int i = 0; i < 5; i ++) {
                try {
                    System.out.println(Thread.currentThread().getName() + ":" + (count++));
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public synchronized void run() {
        method();
    }
}
