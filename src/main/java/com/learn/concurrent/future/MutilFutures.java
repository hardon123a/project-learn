package com.learn.concurrent.future;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.*;

/**
 * https://blog.csdn.net/weixin_44863537/article/details/113354411
 */
public class MutilFutures {
    public static void main(String[] args) throws InterruptedException {
        ExecutorService service = Executors.newFixedThreadPool(15);
        ArrayList<Future> futures = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            Future<Integer> future = service.submit(new CallableTask());
            futures.add(future);
        }

        Thread.sleep(5000);
        for (int i = 0; i < 20; i++) {
            try {
                Future future = futures.get(i);
                Object o = future.get();
                System.out.println(o);
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    static class CallableTask implements Callable<Integer> {
        @Override
        public Integer call() throws Exception {
            Thread.sleep(3000);
            System.out.println(Thread.currentThread().getName()+":正在计算。。。");
            return new Random().nextInt();
        }
    }
}
