package com.learn.algorithm.leetcode;

import com.learn.datastructures.queue.LinkedQueue;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.DelayQueue;

public class T3 {

    public static void main(String[] args) {


        Queue<String> queue=new LinkedList<>();
        queue.offer("test");
        queue.offer("test2");
        queue.offer("test3");
        queue.offer("test4");
        queue.offer("test5");

        //System.out.println(queue.poll());
        System.out.println(lengthOfLongestSubstring("abcddde"));
    }

    public static int lengthOfLongestSubstring(String s) {

        int maxLength = 0; //队列最大长度
        // 创建一个队列
        Queue<Character> queue= new LinkedList<Character>();

        for(int i=0;i<s.length();i++){
            // 判断队列中是否包括当前字符，如果包含就把前面的移除掉，直到不包含为止
            while (queue.contains(s.charAt(i))){
                queue.poll(); //出队
            }

            //把当前字符加入队列中
            queue.offer(s.charAt(i));

            maxLength=Math.max(maxLength,queue.size());
        }

        return maxLength;
    }
}
