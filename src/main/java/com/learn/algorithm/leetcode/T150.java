package com.learn.algorithm.leetcode;

import java.util.Stack;

public class T150 {


    public static void main(String[] args) {
        String[] tokens=new String[]{"2","1","+","3","*"};
        int result=evalRPN(tokens);
        System.out.println(result);
    }

    public static int evalRPN(String[] tokens) {
        Stack stack=new Stack();

        for(int i=0;i<tokens.length;i++){
            if(tokens[i].equals("+")||
                    tokens[i].equals("-")||
                    tokens[i].equals("*")||
                    tokens[i].equals("/")){
                int num1=Integer.parseInt(stack.pop().toString());
                int num2=Integer.parseInt(stack.pop().toString());
                int result=0;
                if(tokens[i].equals("+")){
                    result=num2+num1;
                }
                if(tokens[i].equals("-")){
                    result=num2-num1;
                }

                if(tokens[i].equals("*")){
                    result=num2*num1;
                }

                if(tokens[i].equals("/")){
                    result=num2/num1;
                }

                stack.push(result);

            }else{
                stack.push(tokens[i]);
            }
        }

        return Integer.parseInt(stack.peek().toString());
    }
}
