package com.learn.algorithm.leetcode;

public class T70 {

    public static void main(String[] args) {
        System.out.println(climbStairs(5));
    }

    public static int climbStairs(int n) {

        if(n==1){
            return 1;
        }else if(n==2){
            return 2;
        }

        return climbStairs(n-2)+ climbStairs(n-1);
    }
}


