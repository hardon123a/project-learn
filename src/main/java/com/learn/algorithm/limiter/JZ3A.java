package com.learn.algorithm.limiter;

import java.util.ArrayList;

public class JZ3A {

    ArrayList<Integer> list = new ArrayList();

    public ArrayList<Integer> printListFromTailToHead(ListNode listNode) {
        if (listNode != null) {
            printListFromTailToHead(listNode.next);
            list.add(listNode.val);
        }
        return list;
    }

    public static void main(String[] args) {
        ListNode list1=new ListNode(2);
        ListNode list2=new ListNode(8);
        ListNode list3=new ListNode(1);
        list1.next=list2;
        list2.next=list3;

        JZ3 jz3=new JZ3();
        ArrayList<Integer> list=jz3.printListFromTailToHead(list1);
        System.out.println(list);
    }
}
