package com.learn.algorithm.limiter;

import java.util.ArrayList;
import java.util.List;

public class JZ3 {
    public ArrayList<Integer> printListFromTailToHead(ListNode listNode) {

        ArrayList<Integer> list=new ArrayList<>();
        ListNode tmp=listNode;
        while (tmp!=null){
            list.add(0,tmp.val);
            tmp=tmp.next;
        }

        return list;
    }

    public static void main(String[] args) {

        ListNode list1=new ListNode(2);
        ListNode list2=new ListNode(8);
        ListNode list3=new ListNode(1);
        list1.next=list2;
        list2.next=list3;

        JZ3 jz3=new JZ3();
        ArrayList<Integer> list=jz3.printListFromTailToHead(list1);
        System.out.println(list);
    }


}
class ListNode{
    int val;
    ListNode next=null;

    public ListNode(int val) {
        this.val = val;
    }
}