package com.learn.algorithm.limiter;

public class JZ2 {

    public static String replaceSpace(String s) {
        // write code here
        return s.replaceAll(" ","%20");
    }
    public static String replaceSpace2(String s) {
        // write code here
        StringBuffer sb=new StringBuffer();
        char[] a=s.toCharArray();
        for(char c:a){
            sb.append(c==' '?"%20":c);
        }

        return sb.toString();
    }

    public static void main(String[] args) {

        String s="We Are Happy";
        //System.out.println(replaceSpace(s));
        System.out.println(replaceSpace2(s));
    }
}
