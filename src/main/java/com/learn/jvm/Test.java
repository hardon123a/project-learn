package com.learn.jvm;

import javassist.*;

class Hello {

    public void say() {
        System.out.println("Hello");
    }
}

public class Test {
    public static void main(String[] args) throws NotFoundException, IllegalAccessException, InstantiationException, CannotCompileException {
        ClassPool cp = ClassPool.getDefault();
        CtClass cc = cp.get("com.learn.jvm.Hello");
        CtMethod m = cc.getDeclaredMethod("say");
        m.setBody("{System.out.println(\"shit\");}");
        m.insertBefore("System.out.println(\"fuck\");");
        m.insertAfter("System.out.println(\"fuck2\");");

        Class c = cc.toClass();
        Hello h = (Hello) c.newInstance();
        h.say();
    }
}
