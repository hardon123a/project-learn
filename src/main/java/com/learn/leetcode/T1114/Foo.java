package com.learn.leetcode.T1114;

import java.util.concurrent.Semaphore;

public class Foo {

    private boolean oneDone;
    private boolean twoDone;

    public Foo() {
        oneDone = false;
        twoDone = false;
    }

    public synchronized void first(Runnable printFirst) throws InterruptedException {
        printFirst.run();
        oneDone = true;
        notifyAll();
    }

    public synchronized void second(Runnable printSecond) throws InterruptedException {
        while (!oneDone) {
            wait();
        }
        printSecond.run();
        twoDone = true;
        notifyAll();
    }

    public synchronized void third(Runnable printThird) throws InterruptedException {
        while (!twoDone) {
            wait();
        }
        printThird.run();
    }

    public static void main(String[] args) throws InterruptedException {

        Foo foo=new Foo();

        Thread t1=new Thread(()->{
            System.out.println("first");
        });

        Thread t2=new Thread(()->{
            System.out.println("second");
        });

        Thread t3=new Thread(()->{
            System.out.println("third");
        });

        foo.first(t1);
        foo.second(t2);
        foo.third(t3);

    }
}
