package com.learn.designpattern.builder;

public abstract class HouseBuilder {

    private House house;

    public HouseBuilder(House house) {
        this.house=house;
    }

    //将建造的流程写好, 抽象的方法
    public abstract void buildBasic();
    public abstract void buildWalls();
    public abstract void roofed();

}
