package com.learn.designpattern.builder;

public class HighHouseBuilder extends HouseBuilder{
    public HighHouseBuilder(House house) {
        super(house);
    }

    @Override
    public void buildBasic() {
        System.out.println(" 高楼大厦打地基 5 米 ");
    }
    @Override
    public void buildWalls() {
        System.out.println(" 高楼大厦砌墙 10cm ");
    }
    @Override
    public void roofed() {
        System.out.println(" 高楼大厦封顶 ");
    }
}
