package com.learn.designpattern.builder;

public class Client {
    public static void main(String[] args) {

        //产品
        House house=new House();

        //建造者
        HouseBuilder builder=new CommonHouseBuilder(house);

        //指挥者
        HouseDirector director=new HouseDirector(builder);
        director.buildHouse();

        new HouseDirector(new HighHouseBuilder(house)).buildHouse();

        new HouseDirector(new OtherHouseBuilder(house)).buildHouse();
    }
}
