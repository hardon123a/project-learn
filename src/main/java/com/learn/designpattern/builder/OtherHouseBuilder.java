package com.learn.designpattern.builder;

public class OtherHouseBuilder extends HouseBuilder{
    public OtherHouseBuilder(House house) {
        super(house);
    }

    @Override
    public void buildBasic() {
        System.out.println(" 其他房子打地基 5 米 ");
    }

    @Override
    public void buildWalls() {
        System.out.println(" 其他房子砌墙 10cm ");
    }

    @Override
    public void roofed() {
        System.out.println(" 其他房子封顶 ");
    }
}