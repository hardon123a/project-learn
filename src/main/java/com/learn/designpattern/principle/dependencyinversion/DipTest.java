package com.learn.designpattern.principle.dependencyinversion;
/**
 * 依赖倒置原则 面向接口编程 面向抽象编程
 * //https://zhuanlan.zhihu.com/p/24614363
 */
public class DipTest {

    public static void main(String[] args) {

        /*版本1*/
        /*Learn l=new Learn();
        l.studyJava();
        l.studyPython();
        l.studyAI();*/

        /*版本2*/
        /*Learn l=new Learn();
        l.study(new JavaCourse());
        l.study(new PythonCourse());*/

        /*版本3*/
        /*new Learn(new JavaCourse());
        new Learn(new PythonCourse());*/

        /*版本4*/
        Learn l=new Learn();
        l.setCourse(new JavaCourse());
        l.study();





    }
}
