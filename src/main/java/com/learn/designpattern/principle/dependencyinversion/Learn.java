package com.learn.designpattern.principle.dependencyinversion;
public class Learn {

    private Icourse course;

    /*public void studyJava(){
        System.out.println("Tom正在学习Java");
    }

    public void studyPython(){
        System.out.println("Tom正在学习Python");
    }

    public void studyAI(){
        System.out.println("Tom 正在学习AI");
    }*/

    /*public void study(Icourse course){
        course.study();
    }*/

    /*public Learn(Icourse course) {
        this.course = course;
        this.course.study();
    }*/

    public void setCourse(Icourse course) {
        this.course = course;
    }

    public void study(){
        this.course.study();
    }

}
