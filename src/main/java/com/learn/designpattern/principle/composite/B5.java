package com.learn.designpattern.principle.composite;

public class B5 {
    private A a;

    public B5(A a){
        this.a=a;
    }

    public void operate(){
        a.operate();
    }

}
