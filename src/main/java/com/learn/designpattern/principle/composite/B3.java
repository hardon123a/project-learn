package com.learn.designpattern.principle.composite;

public class B3 {

    private A a;

    public A getA() {
        return a;
    }

    public void setA(A a) {
        this.a = a;
    }

    public void operate(){
        a.operate();
    }

}
