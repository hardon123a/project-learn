package com.learn.designpattern.principle.composite;

public class Test {

    public static void main(String[] args) {

        //1 继承
        B1 b=new B1();
        b.operate();

        //2 方法依赖
        B2 b2=new B2();
        b2.operate(new A());

        //3 属性组合
        B3 b3=new B3();
        b3.setA(new A());
        b3.operate();

        //4 构造函数组合
        A a=new A();
        B5 b5=new B5(a);
        b5.operate();

    }
}
