package com.learn.designpattern.principle.composite;

public class B2 {

    public void operate(A a){
        a.operate();
    }
}
