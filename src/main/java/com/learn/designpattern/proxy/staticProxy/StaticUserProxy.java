package com.learn.designpattern.proxy.staticProxy;

import org.junit.Test;

/**
 * @auther huang jianping
 * @date 2019/7/26 11:59
 */
public class StaticUserProxy {

    @Test
    public void testStaticProxy(){
        //目标对象
        IUserDao target = new UserDao();
        //代理对象
        UserDaoProxy proxy = new UserDaoProxy(target);
        proxy.save();
    }
}
