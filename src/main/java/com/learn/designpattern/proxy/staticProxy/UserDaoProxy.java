package com.learn.designpattern.proxy.staticProxy;

/**
 * @auther huang jianping
 * @date 2019/7/26 11:58
 */
public class UserDaoProxy implements IUserDao{

    private IUserDao target;
    public UserDaoProxy(IUserDao target) {
        this.target = target;
    }

    @Override
    public void save() {
        System.out.println("开启事务");//扩展了额外功能
        target.save();
        System.out.println("提交事务");
    }
}
