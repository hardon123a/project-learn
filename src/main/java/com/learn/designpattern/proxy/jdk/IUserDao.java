package com.learn.designpattern.proxy.jdk;

/**
 * @auther huang jianping
 * @date 2019/7/26 11:57
 */
public interface IUserDao {
    public void save();
}
