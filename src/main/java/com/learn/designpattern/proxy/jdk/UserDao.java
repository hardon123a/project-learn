package com.learn.designpattern.proxy.jdk;

/**
 * @auther huang jianping
 * @date 2019/7/26 11:57
 */
public class UserDao implements IUserDao {

    @Override
    public void save() {
        System.out.println("保存数据");
    }
}
