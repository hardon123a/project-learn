package com.learn.designpattern.proxy.cglib;

/**
 * @auther huang jianping
 * @date 2019/7/26 12:10
 */
public class UserDao {

    public void save() {
        System.out.println("保存数据");
    }
}
