package com.learn.designpattern.chain;

import org.apache.commons.lang.StringUtils;

public class ValidateHandler extends Handler{
    @Override
    public void doHandler(Member member) {
        if(StringUtils.isEmpty(member.getLoginName()) ||
                StringUtils.isEmpty(member.getLoginPass())){
            System.out.println("用户名和密码为空");
            return;
        }
        System.out.println("用户名和密码校验非空，可以进行一下环节");
        next.doHandler(member);
    }
}
