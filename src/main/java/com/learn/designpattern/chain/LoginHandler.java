package com.learn.designpattern.chain;


import org.apache.commons.lang.StringUtils;

public class LoginHandler extends Handler {

    @Override
    public void doHandler(Member member) {
        member.setRoleName("管理员");
        System.out.println("登录成功,并获取用户权限信息，可以进行下一环节");
        next.doHandler(member);
    }
}
