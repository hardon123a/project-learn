package com.learn.designpattern.chain;


public abstract class Handler {

    protected Handler next;

    public void next(Handler next) {
        this.next=next;
    }

    public abstract void doHandler(Member member);
}
