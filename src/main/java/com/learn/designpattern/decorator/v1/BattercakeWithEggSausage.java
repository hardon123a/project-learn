package com.learn.designpattern.decorator.v1;

public class BattercakeWithEggSausage extends BattercakeWithEgg{
    @Override
    protected String getDesc() {
        return super.getDesc()+ " 加1根香肠";
    }

    @Override
    protected int cost() {
        return super.cost()+5;
    }
}
