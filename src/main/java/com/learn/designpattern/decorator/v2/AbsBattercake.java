package com.learn.designpattern.decorator.v2;

public abstract class AbsBattercake {

    protected abstract  String getDesc();

    protected abstract int cost();
}
