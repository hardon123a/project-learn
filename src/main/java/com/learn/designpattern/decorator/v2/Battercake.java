package com.learn.designpattern.decorator.v2;

public class Battercake extends AbsBattercake{
    @Override
    protected String getDesc() {
        return "煎饼";
    }

    @Override
    protected int cost() {
        return 8;
    }
}
