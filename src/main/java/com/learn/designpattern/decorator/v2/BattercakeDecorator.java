package com.learn.designpattern.decorator.v2;

public class BattercakeDecorator extends AbsBattercake{

    private AbsBattercake battercake;

    public BattercakeDecorator(AbsBattercake battercake) {
        this.battercake = battercake;
    }

    @Override
    protected String getDesc() {
        return this.battercake.getDesc();
    }

    @Override
    protected int cost() {
        return this.battercake.cost();
    }
}
