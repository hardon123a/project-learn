package com.learn.designpattern.decorator.v2;


public class Test {
    public static void main(String[] args) {
        AbsBattercake absBattercake;
        absBattercake=new Battercake();
        absBattercake=new EggDecorator(absBattercake);
        absBattercake=new EggDecorator(absBattercake);
        absBattercake=new SausageDecorator(absBattercake);
        absBattercake=new SausageDecorator(absBattercake);
        absBattercake=new SausageDecorator(absBattercake);

        System.out.println(absBattercake.getDesc()+" 销售价格："+absBattercake.cost());



    }
}
