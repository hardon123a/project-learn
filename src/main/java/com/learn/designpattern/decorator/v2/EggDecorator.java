package com.learn.designpattern.decorator.v2;

public class EggDecorator extends BattercakeDecorator{
    public EggDecorator(AbsBattercake battercake) {
        super(battercake);
    }

    @Override
    protected String getDesc() {
        return super.getDesc()+" 加1个鸡蛋";
    }

    @Override
    protected int cost() {
        return super.cost()+1;
    }
}
