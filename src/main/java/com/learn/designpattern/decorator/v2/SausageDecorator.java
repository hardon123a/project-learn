package com.learn.designpattern.decorator.v2;

public class SausageDecorator extends BattercakeDecorator {
    public SausageDecorator(AbsBattercake battercake) {
        super(battercake);
    }

    @Override
    protected String getDesc() {
        return super.getDesc()+" 加1个香肠";
    }

    @Override
    protected int cost() {
        return super.cost()+6;
    }
}