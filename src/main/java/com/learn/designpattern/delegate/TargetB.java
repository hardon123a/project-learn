package com.learn.designpattern.delegate;

public class TargetB implements ITarget{
    @Override
    public void doing(String command) {
        System.out.println("开始开发销售代码");
    }
}
