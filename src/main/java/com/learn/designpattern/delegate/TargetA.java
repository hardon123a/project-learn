package com.learn.designpattern.delegate;

public class TargetA implements ITarget{
    @Override
    public void doing(String command) {
        System.out.println("开始进行加密算法的实现");
    }
}
