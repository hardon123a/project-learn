package com.learn.designpattern.delegate;

public interface ITarget {
    public void doing(String command);
}
