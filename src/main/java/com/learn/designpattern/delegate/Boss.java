package com.learn.designpattern.delegate;

//参考：https://blog.csdn.net/liman65727/article/details/79720352
public class Boss {

    public static void main(String[] args) {
        new Leader().dispatch("加密");
        new Leader().dispatch("销售");
    }
}
