package com.learn.designpattern.delegate;

import java.util.HashMap;
import java.util.Map;

public class Leader {

    private Map<String,ITarget> targets=new HashMap<String,ITarget>();

    public Leader() {
        targets.put("加密",new TargetA());
        targets.put("销售",new TargetB());
    }

    public void dispatch(String command){
        targets.get(command).doing(command);
    }
}
