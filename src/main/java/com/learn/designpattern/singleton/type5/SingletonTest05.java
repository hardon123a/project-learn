package com.learn.designpattern.singleton.type5;

public class SingletonTest05 {
}

// 懒汉式(线程安全，同步方法) 推荐使用
class Singleton{

    private static Singleton instance;

    private Singleton(){}

    //提供一个静态的公有方法，当使用到该方法时，才去创建 instance
    //即懒汉式
    public static synchronized  Singleton getInstance(){

        synchronized (Singleton.class){
            if(instance==null){
                instance=new Singleton();
            }
        }

        return instance;
    }

}