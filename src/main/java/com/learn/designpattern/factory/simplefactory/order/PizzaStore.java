package com.learn.designpattern.factory.simplefactory.order;

public class PizzaStore {

    public static void main(String[] args) {

        //使用简单工厂模式
        new OrderPizza();
    }
}
