package com.learn.designpattern.factory.factorymethod.order;

public class PizzaStore {

    public static void main(String[] args) {
        OrderPizza order1=new BJOrderPizza();
        OrderPizza order2=new LDOrderPizza();
    }
}
