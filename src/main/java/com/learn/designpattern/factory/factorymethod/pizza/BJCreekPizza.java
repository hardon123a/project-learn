package com.learn.designpattern.factory.factorymethod.pizza;

public class BJCreekPizza  extends Pizza{
    @Override
    public void prepare() {
        setName("北京的胡椒pizza");
        System.out.println(" 北京的胡椒pizza 准备原材料");
    }
}
