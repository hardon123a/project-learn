package com.learn.designpattern.factory.factorymethod.pizza;

public class LDChiessPizza extends Pizza{
    @Override
    public void prepare() {
        setName("伦敦的奶酪pizza");
        System.out.println(" 伦敦的奶酪pizza 准备原材料");
    }
}
