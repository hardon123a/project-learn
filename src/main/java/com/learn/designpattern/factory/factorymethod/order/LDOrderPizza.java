package com.learn.designpattern.factory.factorymethod.order;

import com.learn.designpattern.factory.factorymethod.pizza.LDChiessPizza;
import com.learn.designpattern.factory.factorymethod.pizza.LDCreekPizza;
import com.learn.designpattern.factory.factorymethod.pizza.Pizza;

public class LDOrderPizza extends OrderPizza{
    @Override
    Pizza createPizza(String orderType) {
        Pizza pizza = null;
        if(orderType.equals("cheese")) {
            pizza = new LDChiessPizza();
        } else if (orderType.equals("creek")) {
            pizza = new LDCreekPizza();
        }
        return pizza;
    }
}
