package com.learn.designpattern.factory.factorymethod.order;

import com.learn.designpattern.factory.factorymethod.pizza.BJChiessPizza;
import com.learn.designpattern.factory.factorymethod.pizza.BJCreekPizza;
import com.learn.designpattern.factory.factorymethod.pizza.Pizza;

public class BJOrderPizza extends OrderPizza{
    @Override
    Pizza createPizza(String orderType) {
        Pizza pizza = null;
        if(orderType.equals("cheese")) {
            pizza = new BJChiessPizza();
        } else if (orderType.equals("creek")) {
            pizza = new BJCreekPizza();
        }
        return pizza;
    }
}
