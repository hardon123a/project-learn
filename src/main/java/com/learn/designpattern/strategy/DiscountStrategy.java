package com.learn.designpattern.strategy;

public abstract class DiscountStrategy {
    public abstract void shipping();
}
