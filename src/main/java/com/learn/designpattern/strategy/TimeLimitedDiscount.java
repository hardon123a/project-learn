package com.learn.designpattern.strategy;

/**
 * 限时
 */
public class TimeLimitedDiscount extends DiscountStrategy{
    @Override
    public void shipping() {
        System.out.println("享受限时抢购折扣。。。。。。");
    }
}
