package com.learn.designpattern.strategy;

public abstract class PayStrategy {
    public abstract void pay();
}
