package com.learn.designpattern.strategy;

public class Test {
    public static void main(String[] args) {
        PayContext context=new PayContext();

        DiscountStrategy discountStrategy=context.shipment(TimeLimitedDiscount.class);
        discountStrategy.shipping();

        PayStrategy payStrategy=context.payment(AliPay.class);
        payStrategy.pay();
    }
}
