package com.learn.designpattern.strategy;

/**
 * 限量
 */
public class AmountLimitedDiscount extends DiscountStrategy{
    @Override
    public void shipping() {
        System.out.println("享受限量抢购折扣。。。。。。");
    }
}
