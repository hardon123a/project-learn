package com.learn.designpattern.strategy.improve;

public abstract class PayStrategy {
    public abstract void pay();
}
