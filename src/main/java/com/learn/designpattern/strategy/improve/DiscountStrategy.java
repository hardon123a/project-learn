package com.learn.designpattern.strategy.improve;

public abstract class DiscountStrategy {
    public abstract void shipping();
}
