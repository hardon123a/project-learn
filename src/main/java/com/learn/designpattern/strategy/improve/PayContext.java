package com.learn.designpattern.strategy.improve;

public class PayContext {

    private PayStrategy payStrategy;
    private DiscountStrategy discountStrategy;

    public PayContext(PayStrategy payStrategy, DiscountStrategy discountStrategy) {
        this.payStrategy = payStrategy;
        this.discountStrategy = discountStrategy;
        System.out.println("准备支付了。。。。。。");
    }

    /**
     * 支付类型和打折
     */
    public void payment(){

        payStrategy.pay();
        discountStrategy.shipping();
    }


}
