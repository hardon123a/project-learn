package com.learn.designpattern.strategy.improve;
//https://blog.csdn.net/d303577562/article/details/118408304
public class Test {
    public static void main(String[] args) {

        PayStrategy weiXinPay=new WeiXinPay();
        PayStrategy aliPay=new AliPay();

        DiscountStrategy timeLimited=new TimeLimitedDiscount();
        DiscountStrategy amoutLimited=new AmountLimitedDiscount();

        //四种组合
        //1、微信支付+限时折扣
        PayContext context1=new PayContext(weiXinPay,timeLimited);
        context1.payment();
        System.out.println("---------------------------------------------");

        //2、微信支付+限量折扣
        PayContext context2=new PayContext(weiXinPay,amoutLimited);
        context2.payment();
        System.out.println("---------------------------------------------");

        //3、支付宝支付+限时折扣
        PayContext context3=new PayContext(aliPay,timeLimited);
        context3.payment();
        System.out.println("---------------------------------------------");

        //4、支付宝支付+限量折扣
        PayContext context4=new PayContext(aliPay,amoutLimited);
        context4.payment();
        System.out.println("---------------------------------------------");
    }
}
