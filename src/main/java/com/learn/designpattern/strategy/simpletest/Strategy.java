package com.learn.designpattern.strategy.simpletest;

/**
 * 抽象策略类
 */
public interface Strategy {
    void strategyMethod();
}
