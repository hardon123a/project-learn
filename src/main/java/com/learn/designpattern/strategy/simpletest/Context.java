package com.learn.designpattern.strategy.simpletest;

/**
 * 上下文环境类
 */
public class Context {

    private Strategy strategy;

    public Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    /*public Context(Strategy strategy) {
        this.strategy = strategy;
    }*/

    public void strategyMethod(){
        strategy.strategyMethod();
    }
}
