package com.learn.designpattern.strategy.simpletest;

public class Test {

    public static void main(String[] args) {
        Strategy strategy=new ConcreteStrategyA();
        Context context=new Context();
        context.setStrategy(strategy);
        context.strategyMethod();
    }
}
