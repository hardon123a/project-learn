package com.learn.designpattern.strategy;

import com.learn.designpattern.gupao.factory.simplefactory.ICourse;

public class PayContext {

    public PayContext() {
        System.out.println("准备支付了。。。。。。");
    }

    public PayStrategy payment(Class<? extends PayStrategy> clazz){
        try{
            if(null!=clazz)
                return clazz.newInstance();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public DiscountStrategy shipment(Class<? extends DiscountStrategy> clazz){
        try{
            if(null!=clazz)
                return clazz.newInstance();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

}
