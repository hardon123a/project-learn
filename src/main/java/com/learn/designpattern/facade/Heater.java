package com.learn.designpattern.facade;

public class Heater {

    public void open(){
        System.out.println("Heater has been opened!");
    }
}
