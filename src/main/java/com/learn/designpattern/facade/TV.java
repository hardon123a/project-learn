package com.learn.designpattern.facade;

public class TV {

    public void open(){
        System.out.println("TV has been opened!");
    }

    public static void main(String[] args) {

    }

}
