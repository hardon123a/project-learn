package com.learn.designpattern.facade;

public class Light {

    public void open(){
        System.out.println("Light has been opened!");
    }
}
