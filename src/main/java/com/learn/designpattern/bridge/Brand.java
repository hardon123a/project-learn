package com.learn.designpattern.bridge;

//接口
public interface Brand {
    void open();
    void close();
    void call();
}
