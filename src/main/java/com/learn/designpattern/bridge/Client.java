package com.learn.designpattern.bridge;

public class Client {

    public static void main(String[] args) {
        //获取直立式手机（品牌和操作）
        Phone phone=new FoldedPhone(new XiaoMi());
        phone.open();
        phone.call();
        phone.close();

        System.out.println("=================================");

        Phone phone2=new UprightPhone(new Apple()) ;
        phone2.open();
        phone2.call();
        phone2.close();
    }
}
