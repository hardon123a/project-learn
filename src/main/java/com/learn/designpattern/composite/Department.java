package com.learn.designpattern.composite;

public class Department extends OrganizationComponent{
    public Department(String name, String des) {
        super(name, des);
    }

    //没有集合

    @Override
    protected void print() {
        System.out.println(getName());
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public String getDes() {
        return super.getDes();
    }

    @Override
    public void setDes(String des) {
        super.setDes(des);
    }
}
