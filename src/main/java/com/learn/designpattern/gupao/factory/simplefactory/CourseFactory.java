package com.learn.designpattern.gupao.factory.simplefactory;



public class CourseFactory {

    /*public ICourse create(String name){
        if("Java".equals(name)){
            return  new JavaCourse();
        }else if("Python".equals(name)){
            return new PythonCourse();
        }else {
            return null;
        }
    }*/

    public ICourse create(String className){
         try{
             if(!(null==className||"".equals(className)))
             return (ICourse)Class.forName(className).newInstance();
         }catch (Exception e){
             e.printStackTrace();
         }

         return null;
    }

    public ICourse create(Class<? extends ICourse> clazz) {
        try{
            if(null!=clazz)
                return clazz.newInstance();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
