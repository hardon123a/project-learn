package com.learn.designpattern.gupao.factory.factorymethod;

/**
 * 工厂模型
 */
public interface ICourseFactory {
    ICourse create();
}
