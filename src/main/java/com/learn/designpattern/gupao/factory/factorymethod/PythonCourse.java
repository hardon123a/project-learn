package com.learn.designpattern.gupao.factory.factorymethod;

public class PythonCourse implements ICourse{
    @Override
    public void record() {
        System.out.println("录制Python视频");
    }
}
