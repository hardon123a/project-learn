package com.learn.designpattern.gupao.factory.simplefactory;

public interface ICourse {
    /**
     * 录制视频
     */
    void record();
}

