package com.learn.designpattern.gupao.factory.simplefactory;

public class ClientTest {

    public static void main(String[] args) {
       /* CourseFactory factory=new CourseFactory();
        ICourse javaCourse=factory.create("Java");
        javaCourse.record();

        ICourse pythonCourse=factory.create("Python");
        pythonCourse.record();*/

        CourseFactory factory=new CourseFactory();
        ICourse javaCourse=factory.create(JavaCourse.class);
        javaCourse.record();

        ICourse pythonCourse=factory.create(PythonCourse.class);
        pythonCourse.record();
    }
}
