package com.learn.designpattern.observer.event;

public abstract class AbsEvent {

    protected Object source;

    public AbsEvent() {
    }

    public AbsEvent(Object source) {
        this.source = source;
    }
}
