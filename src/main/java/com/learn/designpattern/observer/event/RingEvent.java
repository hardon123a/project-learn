package com.learn.designpattern.observer.event;

import com.learn.designpattern.observer.event.AbsEvent;
import com.learn.designpattern.observer.source.Bell;
import com.learn.designpattern.observer.strategy.RingContext;
import com.learn.designpattern.observer.strategy.RingStrategy;

/**
 * 铃声事件类：用于封装事件源及一些与事件相关的参数
 */
public class RingEvent extends AbsEvent {

    private static final long serialVersionUID = 1L;

    private RingStrategy ringStrategy;  //铃声策略  预备、上课、下课

    public RingEvent(RingStrategy ringStrategy) {
        super();
        this.ringStrategy=ringStrategy;
    }

    public RingEvent(Bell source, RingStrategy ringStrategy) {
        super(source);
        this.ringStrategy=ringStrategy;
    }

    public RingStrategy getRingStrategy() {
        return ringStrategy;
    }

    public void setRingStrategy(RingStrategy ringStrategy) {
        this.ringStrategy = ringStrategy;
    }
}
