package com.learn.designpattern.observer.source;

import com.learn.designpattern.observer.event.AbsEvent;
import com.learn.designpattern.observer.event.RingEvent;
import com.learn.designpattern.observer.listener.AbsEventListener;
import com.learn.designpattern.observer.strategy.RingContext;
import com.learn.designpattern.observer.strategy.RingStrategy;

import java.util.ArrayList;
import java.util.List;

/**
 * 目标类：事件源，铃 观察者
 */
public class Bell extends AbsBell {

    //注册监听事件
    List<AbsEventListener> listeners;//监听器容器


    public Bell() {
        listeners = new ArrayList<>();
    }

    //给事件源绑定监听器
    public void addListener(AbsEventListener listener){
        listeners.add(listener);
    }

    //事件触发器：敲钟，当铃声sound的值发生变化时，触发事件。
    public void ring(Class<? extends RingStrategy> clazz){
        //用策略模式 获取响铃的策略
        RingStrategy strategy=RingContext.getRingStrategy(clazz);
        strategy.ring();//打铃声
        RingEvent e = new RingEvent(strategy);
        //通知注册在该事件源上的所有监听器
        notice(e);
    }

    //当事件发生时,通知绑定在该事件源上的所有监听器做出反应（调用事件处理方法）
    private void notice(RingEvent e){
        for(AbsEventListener listener:listeners){
            listener.heardBell(e);
        }
    }
}
