package com.learn.designpattern.observer.strategy;

/**
 * 响铃策略抽象
 */
public abstract class RingStrategy {
    public abstract void ring();
}
