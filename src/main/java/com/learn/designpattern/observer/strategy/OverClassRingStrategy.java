package com.learn.designpattern.observer.strategy;

public class OverClassRingStrategy extends RingStrategy{
    @Override
    public void ring() {
        System.out.println("下课铃声响！");
    }
}