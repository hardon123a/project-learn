package com.learn.designpattern.observer.strategy;

/**
 * 响铃策略-上课
 */
public class BeginClassRingStrategy extends RingStrategy{
    @Override
    public void ring() {
        System.out.println("上课铃声响！");
    }
}
