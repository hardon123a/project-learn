package com.learn.designpattern.observer.strategy;

import com.learn.designpattern.strategy.PayStrategy;

/**
 * 响铃策略上下文
 */
public class RingContext {

    public static  RingStrategy getRingStrategy(Class<? extends RingStrategy> clazz){
        try{
            if(null!=clazz)
                return clazz.newInstance();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
