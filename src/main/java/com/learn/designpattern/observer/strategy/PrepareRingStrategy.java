package com.learn.designpattern.observer.strategy;

/**
 * 响铃策略-预备
 */
public class PrepareRingStrategy extends RingStrategy{
    @Override
    public void ring() {
        System.out.println("预备铃声响！");
    }
}
