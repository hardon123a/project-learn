package com.learn.designpattern.observer;

import com.learn.designpattern.observer.listener.StuEventListener;
import com.learn.designpattern.observer.listener.TeachEventListener;
import com.learn.designpattern.observer.source.Bell;
import com.learn.designpattern.observer.strategy.BeginClassRingStrategy;
import com.learn.designpattern.observer.strategy.OverClassRingStrategy;
import com.learn.designpattern.observer.strategy.PrepareRingStrategy;

public class ObserverPatternTest {

    public static void main(String[] args) {
        Bell bell = new Bell();    //铃（事件源）
        bell.addListener(new TeachEventListener()); //注册监听器（老师）
        bell.addListener(new StuEventListener());    //注册监听器（学生）
        bell.ring(PrepareRingStrategy.class);    //打预备铃声
        System.out.println("------------");
        bell.ring(BeginClassRingStrategy.class); //打上课铃声
        System.out.println("------------");
        bell.ring(OverClassRingStrategy.class);   //打下课铃声
    }
}
