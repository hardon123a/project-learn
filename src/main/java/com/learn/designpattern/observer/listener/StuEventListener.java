package com.learn.designpattern.observer.listener;

import com.learn.designpattern.observer.event.RingEvent;
import com.learn.designpattern.observer.strategy.BeginClassRingStrategy;
import com.learn.designpattern.observer.strategy.PrepareRingStrategy;

/**
 * 具体观察者类：学生事件监听器
 */
public class StuEventListener extends AbsEventListener{

    @Override
    public void heardBell(RingEvent e) {
        if(e.getRingStrategy() instanceof PrepareRingStrategy) {
            System.out.println("学生准备上课...");
        } else if(e.getRingStrategy() instanceof BeginClassRingStrategy) {
            System.out.println("学生上课了...");
        }else {
            System.out.println("学生下课了...");
        }
    }
}
