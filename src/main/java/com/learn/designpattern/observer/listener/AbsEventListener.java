package com.learn.designpattern.observer.listener;

import com.learn.designpattern.observer.event.RingEvent;

/**
 * 抽象事件监听器
 */
public abstract class AbsEventListener implements Listener{
    //监听器（监听者做出的响应）
    public abstract void heardBell(RingEvent e);

}
