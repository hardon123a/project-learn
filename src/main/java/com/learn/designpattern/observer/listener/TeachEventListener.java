package com.learn.designpattern.observer.listener;

import com.learn.designpattern.observer.event.RingEvent;
import com.learn.designpattern.observer.strategy.BeginClassRingStrategy;
import com.learn.designpattern.observer.strategy.PrepareRingStrategy;

/**
 * 具体观察者类：老师事件监听器
 */
public class TeachEventListener extends AbsEventListener{
    @Override
    public void heardBell(RingEvent e) {
        if(e.getRingStrategy() instanceof PrepareRingStrategy) {
            System.out.println("老师准备上课...");
        } else if(e.getRingStrategy() instanceof BeginClassRingStrategy) {
            System.out.println("老师上课了...");
        }else {
            System.out.println("老师下课了...");
        }
    }
}
