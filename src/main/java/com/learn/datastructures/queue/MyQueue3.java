package com.learn.datastructures.queue;

import java.util.Stack;

/**
 * 利用两个栈来模拟队列操作
 * 入队操作就只是想栈s1中添加，
 * 出栈操作分为两部分：
 * 1.当s2中不为空的时候，就直接弹出s2中的栈顶数据
 * 2.当s2中为空的时候，就先把s1中的数据全部弹出到s2中然后将栈顶数据出栈
 * https://blog.csdn.net/Hellowenpan/article/details/82781767
 * */
public class MyQueue3<E> {

    public static void main(String[] args) {
        MyQueue3<Integer> test=new MyQueue3<Integer>();
        test.put(56);
        test.put(86);
        test.put(556);
        test.put(516);
        test.put(356);

        System.out.println(test.pop());
        System.out.println(test.pop());
        System.out.println(test.pop());
    }

    Stack<E> s1=new Stack<E>();
    Stack<E> s2=new Stack<E>();

    //使用同步处理，保证线程安全
    public synchronized void put(E data){
        s1.push(data);
    }

    public synchronized E pop(){
        if(!s2.isEmpty()){
            return s2.pop();
        }else{
            for(int i=0;i<s1.size();i++) {
                s2.push(s1.pop());
            }
            return s2.pop();
        }
    }

    public synchronized boolean isEmpty(){
        return s1.isEmpty()&&s2.empty();
    }
}
