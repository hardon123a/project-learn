package com.learn.datastructures.sort;

public class BubbleSort {

    public static void main(String[] args) {
        int[] array=new int[]{3,9,-1,10,20};
        bubbleSort(array);
    }

    public static int[] bubbleSort(int[] array){
        for(int i=0;i<array.length-1;i++){
            System.out.println("第"+(i+1)+"趟:");
            for(int j=0;j<array.length-i-1;j++){
                if(array[j]>array[j+1]){
                    int temp=array[j];
                    array[j]=array[j+1];
                    array[j+1]=temp;
                }
                arrayPrint(array);
                System.out.println();
            }
            System.out.println("--------------------------------");
        }
        return array;
    }

    public static void arrayPrint(int[] array){

        for (int i = 0; i <array.length ; i++) {
            System.out.printf(array[i]+" ");
        }
    }
}
