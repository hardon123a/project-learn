package com.learn.datastructures;

public class GetNum {
    public static void main(String[] args) {
        int[] num=new int[]{56,12,11,56,90,23,9,51};

        for (int i = 0; i <num.length ; i++) {
            System.out.println(num[i]);
        }
        System.out.println("---------------------------");

        for(int i=0;i <num.length-1 ; i++){
            if(num[i]>num[i+1]) {
                num[i + 1] = num[i];
            }
        }

        int max=num[num.length-1];
        System.out.println(max);

        System.out.println("---------------------------");

        for(int i=0;i <num.length-1 ; i++){
            if(num[i]>num[i+1]) {
                int temp=num[i + 1];
                num[i + 1] = num[i];
                num[i]=temp;
            }
        }
        for (int i = 0; i <num.length ; i++) {
            System.out.println(num[i]);
        }
        System.out.println("---------------------------");
    }
}
