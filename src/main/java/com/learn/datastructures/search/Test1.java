package com.learn.datastructures.search;

import java.math.BigDecimal;
import java.util.Scanner;

public class Test1 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int[][] num = new int[n][m];
        //初始化数组
        for(int i=0;i<n;i++){
            for(int j=0;j<m;j++){
                num[i][j] = sc.nextInt();
            }
        }
        //记得题干中约束了m和n，此处不加大概也行
        if(m<2||n<2){
            System.out.println(0);
            return;
        }
        BigDecimal ans = new BigDecimal(0);
        int maxLength = Math.min(n,m);
        //三层遍历：遍历所以四个角的组合
        for(int k=1;k<maxLength;k++){
            for(int i=0;i<n;i++){
                //i+k>=n会越界，提前结束
                if(i+k>=n) break;
                for(int j=0;j<m;j++){
                    //j+k>=m会越界，提前结束
                    if(j+k>=m) break;
                    //求和，求最大
                    BigDecimal sum = new BigDecimal(0);
                    sum = sum.add(new BigDecimal(num[i][j]));
                    sum = sum.add(new BigDecimal(num[i][j+k]));
                    sum = sum.add(new BigDecimal(num[i+k][j]));
                    sum = sum.add(new BigDecimal(num[i+k][j+k]));
                    if(sum.compareTo(ans)>0){
                        ans = sum;
                    }
                }
            }
        }
        System.out.println(ans);
    }

}
